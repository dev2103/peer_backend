import { UserService } from './user.service';
import { SmsService } from 'src/sms/sms.service';
import { AuthModule } from '../auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './user.controller';
import { userModelDefinition } from './user.model';
import { Module, forwardRef } from '@nestjs/common';
import { TwilioModule } from '@lkaric/twilio-nestjs';
import { getTwillioOptions } from 'src/utils/env.utils';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TwilioModule.register(getTwillioOptions()),
    MongooseModule.forFeature([userModelDefinition])
  ],
  controllers: [UserController],
  providers: [UserService, SmsService],
  exports: [UserService]
})
export class UserModule {}
