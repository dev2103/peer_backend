import {
  Get,
  Req,
  Put,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Controller,
  UploadedFile,
  UseInterceptors
} from '@nestjs/common';
import {
  ApiTags,
  ApiConsumes,
  ApiBearerAuth,
  ApiOkResponse
} from '@nestjs/swagger';

import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

import { apiResponse } from '../utils/response.utils';
import { responseDescription } from '../utils/swagger.utils';
import { makeInterceptor } from '../utils/file-uploading.utils';

import { UserUpdateDto } from './dto/userUpdate.dto';
import { UserFollowDto } from './dto/userFollow.dto';
import { UserDeleteDto } from './dto/userDelete.dto';
import { UserContactsImportDto } from './dto/userContactsImport';

const NOT_FOUND = 'User is not found';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  //! REMOVE ME IN PROD
  @Get()
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'List of all users'
    })
  )
  getAllUser() {
    return apiResponse(() => this.userService.getUsers());
  }

  //! REMOVE ME IN PROD
  @Get('new/:phone')
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'A single user',
      NOT_FOUND
    })
  )
  newUser(@Param('phone') phoneNumber: string) {
    return apiResponse(() => this.userService.createUser(phoneNumber));
  }

  @Get(':id')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'A single user',
      NOT_FOUND
    })
  )
  getUser(@Param('id') userId: string) {
    return apiResponse(() => this.userService.getSingleUser(userId));
  }

  @Post('/follow')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse(
    responseDescription({
      SUCCESS: "The followed user's profile"
    })
  )
  followUser(@Body() followDto: UserFollowDto, @Req() { user }: any) {
    return apiResponse(() =>
      this.userService.followUser(user.id, followDto.peerId)
    );
  }

  @Post('/import')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'List of contacts that are peer users'
    })
  )
  VerifyListContact(
    @Body() contactsDto: UserContactsImportDto,
    @Req() { user }: any
  ) {
    return apiResponse(() =>
      this.userService.findUserPeers(user.id, contactsDto.phones)
    );
  }

  @Put()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(makeInterceptor('avatar'))
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'User update success',
      NOT_FOUND
    })
  )
  async updateUser(
    @Body() newUser: UserUpdateDto,
    @UploadedFile() avatar: any,
    @Req() { user }: any
  ) {
    return apiResponse(() =>
      this.userService.updateUser(user.id, newUser, avatar?.path)
    );
  }

  @Delete()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'User deletion success',
      NOT_FOUND
    })
  )
  removeUser(@Body() userDeleteDto: UserDeleteDto, @Req() { user }: any) {
    return apiResponse(() =>
      this.userService.deleteUser(user.id, userDeleteDto.code)
    );
  }
}
