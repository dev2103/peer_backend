import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty } from 'class-validator';

export class UserContactsImportDto {
  @IsArray()
  @IsNotEmpty()
  @ApiProperty({
    type: [String],
    description: 'List of phone numbers'
  })
  readonly phones!: string[];
}
