import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class UserUpdateDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    type: String,
    required: false,
    description: "User's full name"
  })
  readonly full_name!: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    type: String,
    required: false,
    enum: ['F', 'M'],
    description: "User's gender"
  })
  readonly gender!: string;

  @IsNotEmpty()
  @ApiProperty({
    type: 'file',
    required: false,
    description: "User's avatar"
  })
  readonly avatar?: any;
}
