import { getEnv } from 'src/utils/env.utils';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Prop, Schema, SchemaFactory, ModelDefinition } from '@nestjs/mongoose';

const genders = ['F', 'M'];

const mutateAvatar = (avatar: string, domain: string) =>
  avatar
    ? avatar.search(domain) === -1
      ? `${domain}/${avatar}`
      : avatar
    : `${domain}/files/default.png`;

const addDomain = (doc: any) => {
  const { APP_DOMAIN } = getEnv();
  doc.avatar = mutateAvatar(doc.avatar, APP_DOMAIN);
  return doc;
};

@Schema()
class User extends Document {
  @Prop({
    type: String
  })
  id!: string;

  @Prop({
    type: String
  })
  avatar!: string;

  @Prop({
    type: String
  })
  fcm_token!: string;

  @Prop({
    type: String,
    required: true,
    enum: genders
  })
  gender!: string;

  @Prop({
    type: String,
    required: true
  })
  full_name!: string;

  @Prop({
    type: String,
    unique: true,
    required: true
  })
  phone_number!: string;

  @Prop({
    type: String,
    unique: true,
    required: true
  })
  backupKey!: string;

  @Prop([
    {
      type: MongooseSchema.Types.ObjectId,
      ref: 'User'
    }
  ])
  followers!: MongooseSchema.Types.ObjectId[];

  @Prop([
    {
      type: MongooseSchema.Types.ObjectId,
      ref: 'User'
    }
  ])
  following!: MongooseSchema.Types.ObjectId[];
}

const UserSchema = SchemaFactory.createForClass(User);

UserSchema.post('init', addDomain);
UserSchema.post('save', addDomain);

const userModelDefinition: ModelDefinition = {
  name: 'User',
  schema: UserSchema
};

export { User, UserSchema, userModelDefinition };
