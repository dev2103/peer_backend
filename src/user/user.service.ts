import * as cuid from 'cuid';
import { Subject } from 'rxjs';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Inject, forwardRef, Injectable } from '@nestjs/common';
import { SmsService } from 'src/sms/sms.service';
import { AuthService } from 'src/auth/auth.service';
import { Clean, cleanObject } from 'src/decorators/clean.decorator';
import { deleteOldAvatar } from 'src/utils/file-uploading.utils';
import { responseStatuses } from 'src/utils/response.utils';
import { UserUpdateDto } from './dto/userUpdate.dto';
import { User } from './user.model';

const { BAD_REQUEST, NOT_FOUND, INVALID_CODE } = responseStatuses;

@Injectable()
export class UserService {
  static eventStream = new Subject<any>();

  constructor(
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
    @Inject(forwardRef(() => SmsService))
    private readonly smsService: SmsService,
    @InjectModel('User')
    private readonly userModel: Model<User>
  ) {}

  private inform(userId: string) {
    UserService.eventStream.next(userId);
  }

  private async findUser(id: string) {
    try {
      const user = await this.userModel.findById(id).exec();

      if (!user) {
        throw new Error();
      }

      return user;
    } catch (error) {
      throw {
        code: NOT_FOUND
      };
    }
  }

  private saveFollowing(userId: any, following: any[]) {
    return this.userModel.updateOne(
      {
        _id: userId
      },
      {
        $addToSet: {
          following: {
            $each: following
          }
        }
      }
    );
  }

  private saveFollowers(userId: any, following: any[]) {
    return this.userModel.updateMany(
      {
        _id: {
          $in: following
        }
      },
      {
        $addToSet: {
          followers: userId
        }
      },
      {
        multi: true
      }
    );
  }

  private preserveRelations(userId: string, following: User[]) {
    const ids = following.map(({ _id }) => _id);

    return Promise.all([
      this.saveFollowing(userId, ids),
      this.saveFollowers(userId, ids)
    ]);
  }

  private UpdatFollower(peer: any, userId: string) {
    if (!peer.followers.find((id: any) => id == userId)) {
      peer.followers.push(userId);
    }

    return cleanObject(peer);
  }

  private async updateRelations(userId: string, following: User[]) {
    await this.preserveRelations(userId, following);

    return following.map(peer => this.UpdatFollower(peer, userId));
  }

  private getUserContacts(userId: string, phones: string[]) {
    return this.userModel
      .find({
        _id: {
          $ne: userId
        },
        phone_number: {
          $in: phones
        }
      })
      .exec();
  }

  private updateBackupKey(phone_number: string, backupKey: string) {
    return this.userModel.update(
      {
        phone_number
      },
      {
        $set: {
          backupKey
        }
      }
    );
  }

  private async isDuplicateKey(backupKey: string) {
    const count = await this.userModel.count({ backupKey });
    return count > 0;
  }

  private async generateBackupKey(phone_number: string) {
    const currentKey = cuid();

    if (await this.isDuplicateKey(currentKey)) {
      return this.generateBackupKey(phone_number);
    }

    await this.updateBackupKey(phone_number, currentKey);

    return currentKey;
  }

  async getUsers() {
    const users = await this.userModel.find().exec();

    return users?.map(user => this.authService.generateJWT(user));
  }

  @Clean()
  getSingleUser(userId: string) {
    return this.findUser(userId);
  }

  @Clean()
  async followUser(userId: string, peerId: string) {
    if (peerId === userId) {
      throw {
        code: BAD_REQUEST
      };
    }

    const peer = await this.findUser(peerId);

    await this.updateRelations(userId, [peer]);

    this.inform(userId);

    return await this.findUser(peerId);
  }

  private isDefaultAvatar(avatar: string) {
    return avatar.split('/').reverse()[0] === 'default.png';
  }

  async findUserPeers(userId: string, phones: string[]) {
    if (!phones.length) {
      throw {
        code: BAD_REQUEST
      };
    }

    const following = await this.getUserContacts(userId, phones);

    const peers = await this.updateRelations(userId, following);

    this.inform(userId);

    return peers;
  }

  getUserByPhoneNumber(phone_number: string) {
    return this.userModel.findOne({ phone_number }).exec();
  }

  async createUser(phone_number: string) {
    const gender = 'M';
    const full_name = 'Anonymous user';
    const backupKey = await this.generateBackupKey(phone_number);

    const doc = new this.userModel({
      phone_number,
      full_name,
      backupKey,
      gender
    });

    const newUser = await doc.save();

    return this.authService.generateJWT(newUser);
  }

  @Clean()
  async updateUser(
    id: string,
    { full_name, gender }: UserUpdateDto,
    avatar: string
  ) {
    const user = await this.findUser(id);

    if (full_name) {
      user.full_name = full_name;
    }

    if (gender) {
      if (gender !== 'M' && gender !== 'F') {
        throw {
          code: BAD_REQUEST
        };
      }

      user.gender = gender;
    }

    if (avatar && !this.isDefaultAvatar(avatar)) {
      deleteOldAvatar(user.avatar);
      user.avatar = avatar;
    }

    return await user.save();
  }

  async deleteUser(userId: string, code: string) {
    const { phone_number } = await this.getSingleUser(userId);

    const isVerified = await this.smsService.isValidVerification(
      phone_number,
      code
    );

    if (!isVerified) {
      throw {
        code: INVALID_CODE
      };
    }

    await this.delete(userId);
  }

  private async delete(userId: string) {
    const deleted = await this.userModel
      .findOneAndRemove({ _id: userId })
      .exec();

    if (!deleted) {
      throw {
        code: NOT_FOUND
      };
    }

    await this.unlinkPeers(deleted);
  }

  private async unlinkPeers(user: User) {
    const unlink = async (type: boolean) => {
      const types = ['following', 'followers'];
      const from = types[+type];
      const to = types[+!type];

      const peers = await this.userModel.find({
        _id: {
          $in: user[from]
        }
      });

      return peers.map((peer: any) => {
        peer[to].pull(user._id);
        return peer.save();
      });
    };

    const unlinkPromises = new Array(2).fill(0).map((_, i) => unlink(!!i));

    const peersPromises = await Promise.all(unlinkPromises);

    await Promise.all(peersPromises.flat());
  }

  // public sendNotifications(json): void {
  //     const method = 'POST';
  //     const uri = 'https://fcm.googleapis.com/fcm/send';
  //     const headers = {
  //         Authorization:
  //             'key=AAAAh-rqL7I:APA91bFgmnHjqddZWSw0jx_-GISbUoZU-v3RyBOPc-0hTvPPG-29IPziGetktgeojz4KLum98DXpQIp0udMGcvp6t91_ZsFeANDOyoSmF8UepC5qG7-wBN6ClaMOjqBfXfnhoIQQ2sCG',
  //         'Content-Type': 'application/json',
  //         'Content-Length': JSON.stringify(json).length,
  //     };

  //     const options = {
  //         uri,
  //         json,
  //         method,
  //         headers
  //     };

  //     const callback = (err, res, body): void => {
  //         if (err) throw err;
  //         else console.log(body);
  //     };

  //     request(options, callback);
  // };
}
