import { Cache } from 'cache-manager';
import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';

const ONLINE = 'ONLINE';

@Injectable()
export class RedisCacheService {
  constructor(@Inject(CACHE_MANAGER) private readonly cache: Cache) {}

  private set(key: string, value: string) {
    return this.cache.set(key, value, { ttl: 0 });
  }

  login(id: string) {
    return this.set(id, ONLINE);
  }

  logout(id: string) {
    return this.set(id, new Date().toISOString());
  }

  status(id: string) {
    return this.cache.get(id);
  }

  get all() {
    return this.cache.store.keys ? this.cache.store.keys() : [];
  }
}
