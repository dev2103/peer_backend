import { JwtService } from '@nestjs/jwt';
import { User } from '../user/user.model';
import { Injectable } from '@nestjs/common';
import { cleanObject } from '../decorators/clean.decorator';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  generateJWT(newUser: User): User {
    const { id, ...user } = cleanObject(newUser, true);
    const token = this.jwtService.sign({ id });

    return { ...user, id, token };
  }
}
