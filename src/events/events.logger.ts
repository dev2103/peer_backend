import { Logger } from '@nestjs/common';

export class EventsLogger extends Logger {
  constructor() {
    super('AppGateway');
  }

  logError(type: string, id: string) {
    this.error(`404 | ${type} | ID => ${id}`);
  }

  logInfo(type: string, id: string) {
    this.warn(`:::${type}::: | ID => ${id}`);
  }

  logInform(type: string, id: string) {
    this.log(`INFORMED ${type} => ${id}`);
  }

  logEvent(event: string, data: any) {
    const e = event.toUpperCase();
    const d = JSON.stringify(data, null, 4);

    this.log(`** ${e} =>\n${d}`);
  }

  logPeers(peers: any[]) {
    this.logEvent('peers', peers);
  }

  peerNotFound(id: string) {
    this.logError('PEER_NOT_FOUND', id);
  }

  socketNotFound(id: string) {
    this.logError('SOCKET_NOT_FOUND', id);
  }

  socketNotAssociated(id: string) {
    this.logError('NO_SOCKET_ASSOCIATED', id);
  }

  connected(id: string) {
    this.logInfo('CONNECT', id);
  }

  disconnected(id: string) {
    this.logInfo('DISCONNECT', id);
  }

  init(path: string) {
    this.log(`Initialized socket server on => ${path}`);
  }
}
