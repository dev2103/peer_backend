import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { RedisCacheService } from '../redis-cache/redis-cache.service';

@Injectable()
export class EventsService {
  private readonly peerSocketMap: Map<string, string> = new Map();

  constructor(
    private readonly userService: UserService,
    private readonly redisCacheService: RedisCacheService
  ) {}

  login(peerId: string, socketId: string) {
    this.peerSocketMap.set(peerId, socketId);
    this.redisCacheService.login(peerId);
  }

  logout(peerId: string) {
    this.peerSocketMap.delete(peerId);
    this.redisCacheService.logout(peerId);
  }

  checkPeer(peerId: string) {
    return this.peerSocketMap.has(peerId);
  }

  getSocket(peerId: string) {
    return this.peerSocketMap.get(peerId);
  }

  getPeer(socketId: string) {
    for (let [key, value] of this.peerSocketMap.entries()) {
      if (value === socketId) {
        return key;
      }
    }
  }

  formatPeers() {
    return [...this.peerSocketMap.entries()].map(([peer, socket]) => ({
      peer,
      socket
    }));
  }

  private getPeerStatus = async (peerId: string) => {
    const status: string = await this.redisCacheService.status(peerId);

    return [peerId, status];
  };

  private async getPeersStatuses() {
    const peers: string[] = await this.redisCacheService.all;
    const statuses = await Promise.all(peers.map(this.getPeerStatus));

    return Object.fromEntries(statuses);
  }

  async getStatus() {
    const peers = [...this.peerSocketMap.keys()];

    const statuses = await this.getPeersStatuses();

    return {
      peers,
      statuses
    };
  }

  async getFollowers(peerId: string) {
    const { followers } = await this.userService.getSingleUser(peerId);
    return followers;
  }
}
