import { Module, forwardRef } from '@nestjs/common';
import { EventsGateway } from './events.gateway';
import { EventsService } from './events.service';
import { RedisCacheModule } from '../redis-cache/redis-cache.module';
import { UserModule } from '../user/user.module';

@Module({
  imports: [RedisCacheModule, forwardRef(() => UserModule)],
  providers: [EventsGateway, EventsService],
  exports: [EventsService, RedisCacheModule]
})
export class EventsModule {}
