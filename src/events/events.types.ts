export interface CallRequest {
  id: string;
  caller: string;
  callee: string;
  type: 'video' | 'audio';
}

export interface CallResponse {
  id: string;
  caller: string;
  callee: string;
  response: 0 | 1;
  type: 'video' | 'audio';
}

export interface CallCancel {
  to: string;
  sessionId: string;
}

export interface CallEnd {
  peerId: string;
  sessionId: string;
  reasonCode: number;
}

// Constants
export const NEW = 'new';
export const BYE = 'bye';
export const PEERS = 'peers';
export const OFFER = 'offer';
export const ANSWER = 'answer';
export const CALL_END = 'end_call';
export const CANDIDATE = 'candidate';
export const CALL_CANCEL = 'call_cancel';
export const CALL_REQUEST = 'call_request';
export const CALL_RESPONSE = 'call_response';
