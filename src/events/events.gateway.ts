import {
  NEW,
  BYE,
  PEERS,
  OFFER,
  ANSWER,
  CALL_END,
  CANDIDATE,
  CALL_CANCEL,
  CALL_REQUEST,
  CALL_RESPONSE,
  CallResponse,
  CallRequest,
  CallCancel,
  CallEnd
} from './events.types';
import {
  MessageBody,
  OnGatewayInit,
  ConnectedSocket,
  WebSocketServer,
  WebSocketGateway,
  SubscribeMessage,
  OnGatewayConnection,
  OnGatewayDisconnect
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { EventsLogger } from './events.logger';
import { EventsService } from './events.service';
import { UserService } from 'src/user/user.service';

@WebSocketGateway()
export class EventsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  private readonly server: Server | undefined;

  private readonly eventsLogger = new EventsLogger();

  constructor(private readonly eventsService: EventsService) {
    UserService.eventStream.subscribe(userId => this.inform(userId));
  }

  afterInit(server: Server) {
    this.eventsLogger.init(server.path());
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.eventsLogger.connected(client.id);
  }

  async handleDisconnect(client: Socket) {
    const peer = this.getPeer(client.id);

    if (peer) {
      this.eventsLogger.disconnected(peer);

      this.eventsService.logout(peer);

      try {
        await this.informFollowers(peer);
      } catch (err) {
        console.error(err);
      }
    }
  }

  private emit(peerId: string, type: string, data: any): boolean {
    const socket: Socket | null = this.getSocket(peerId);

    if (!socket) {
      return false;
    }

    socket.emit('message', JSON.stringify({ type, data }));

    return true;
  }

  private parseData(event: string, data: any) {
    const parsed = JSON.parse(data);
    this.eventsLogger.logEvent(event, parsed);
    return parsed;
  }

  private logPeers() {
    const peers = this.eventsService.formatPeers();
    this.eventsLogger.logPeers(peers);
  }

  private getPeer(socketId: string): string | null {
    const peer: string | undefined = this.eventsService.getPeer(socketId);

    if (!peer) {
      this.eventsLogger.peerNotFound(socketId);
      return null;
    }

    return peer;
  }

  private getSocket(peerId: string): Socket | null {
    if (!this.eventsService.checkPeer(peerId)) {
      this.eventsLogger.peerNotFound(peerId);
      return null;
    }

    const socketId = this.eventsService.getSocket(peerId);

    if (!socketId) {
      this.eventsLogger.socketNotAssociated(peerId);
      return null;
    }

    const socket = this.server?.sockets.connected[socketId];

    if (!socket) {
      this.eventsLogger.socketNotFound(peerId);
      return null;
    }

    return socket;
  }

  async informFollowers(self: string) {
    const [followers, { peers, statuses }] = await Promise.all([
      this.eventsService.getFollowers(self),
      this.eventsService.getStatus()
    ]);

    const payload = { statuses, peers, self };

    followers.forEach(peerId => {
      if (this.emit(peerId.toString(), PEERS, payload)) {
        this.eventsLogger.logInform('FOLLOWER', peerId.toString());
      }
    });
  }

  private async informUser(self: string) {
    const { peers, statuses } = await this.eventsService.getStatus();
    const payload = { statuses, peers, self };

    if (this.emit(self, PEERS, payload)) {
      this.eventsLogger.logInfo('SELF', self);
    }
  }

  async inform(id: string) {
    try {
      return await Promise.allSettled([
        this.informFollowers(id),
        this.informUser(id)
      ]);
    } catch (error) {
      console.error(error);
    }
  }

  @SubscribeMessage(BYE)
  async onByEvent(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const byeData = this.parseData(BYE, data);

    const { from: to } = byeData;

    this.emit(to, BYE, { to });
  }

  @SubscribeMessage(OFFER)
  async onOfferEvent(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const offerData = this.parseData(OFFER, data);

    this.emit(offerData.to, OFFER, offerData);
  }

  @SubscribeMessage(NEW)
  async onNewEvent(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const newData = this.parseData(NEW, data);

    const { id } = newData;

    this.eventsService.login(id, client.id);

    await this.inform(id);
  }

  @SubscribeMessage(ANSWER)
  async onAnswerEvent(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const answerData = this.parseData(ANSWER, data);

    this.emit(answerData.to, ANSWER, answerData);
  }

  @SubscribeMessage(CANDIDATE)
  async onIceCandidateEvent(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const candidateData = this.parseData(CANDIDATE, data);

    this.emit(candidateData.to, CANDIDATE, candidateData);
  }

  @SubscribeMessage(CALL_REQUEST)
  async onCallRequest(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const callRequest: CallRequest = this.parseData(CALL_REQUEST, data);

    this.emit(callRequest.callee, CALL_REQUEST, callRequest);
  }

  @SubscribeMessage(CALL_END)
  async onEndCall(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const callEnd: CallEnd = this.parseData(CALL_END, data);

    this.emit(callEnd.peerId, CALL_END, callEnd);
  }

  @SubscribeMessage(CALL_RESPONSE)
  async onCallResponse(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const callResponse: CallResponse = this.parseData(CALL_RESPONSE, data);

    this.emit(callResponse.caller, CALL_RESPONSE, callResponse);
  }

  @SubscribeMessage(CALL_CANCEL)
  async onCallCancel(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any
  ): Promise<void> {
    const callCancel: CallCancel = this.parseData(CALL_CANCEL, data);

    const { to, sessionId } = callCancel;

    this.emit(to, CALL_CANCEL, { sessionId });
  }
}
