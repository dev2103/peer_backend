import {
  IsString,
  MinLength,
  MaxLength,
  IsNotEmpty,
  IsPhoneNumber
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class smsSendDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(7)
  @MaxLength(15)
  @IsPhoneNumber('ZZ')
  @ApiProperty({
    type: String,
    description: 'Phone number to recieve the verification code'
  })
  readonly phoneNumber!: string;
}
