import {
  IsString,
  MinLength,
  MaxLength,
  IsNotEmpty,
  IsPhoneNumber
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class smsCheckDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(7)
  @MaxLength(15)
  @IsPhoneNumber('ZZ')
  @ApiProperty({
    type: String,
    description: 'The phone number that recieved the code'
  })
  readonly phoneNumber!: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: 'The code to verify'
  })
  readonly code!: string;
}
