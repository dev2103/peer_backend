import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UserService } from 'src/user/user.service';
import { AuthService } from 'src/auth/auth.service';
import { TwilioService } from '@lkaric/twilio-nestjs';
import { responseStatuses } from 'src/utils/response.utils';
import { ServiceContext } from 'twilio/lib/rest/verify/v2/service';

// Peer Statuses
const {
  FAILURE,
  INVALID_CODE,
  INVALID_NUMBER,
  DUPLICATE_PHONE
} = responseStatuses;

// Twilio Statuses
const BAD_REQUEST = '400';
const PENDING = 'pending';
const APPROVED = 'approved';

@Injectable()
export class SmsService {
  private readonly twillioServiceCtx: ServiceContext;

  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly twilioService: TwilioService,
    private readonly configService: ConfigService
  ) {
    const key = this.configService.get('TWILLIO_SERVICE') as string;
    const sid = this.configService.get('TWILLIO_RATE_LIMIT') as string;

    this.twillioServiceCtx = this.twilioService.client.verify.services(key);

    this.twillioServiceCtx.rateLimits(sid).buckets.create({
      // Maximum Five times per day
      interval: 60 * 60 * 24,
      max: 5
    });
  }

  private async send(phoneNumber: string) {
    const channel = 'sms';
    const to = `+${phoneNumber}`;

    return this.twillioServiceCtx.verifications.create({
      to,
      channel,
      rateLimits: {
        phoneNumber: to
      }
    });
  }

  private verify(phoneNumber: string, code: string) {
    const to = `+${phoneNumber}`;

    return this.twillioServiceCtx.verificationChecks.create({
      code,
      to
    });
  }

  private async checkForDuplicate(phoneNumber: string) {
    const user = await this.userService.getUserByPhoneNumber(phoneNumber);

    if (user) {
      const code = DUPLICATE_PHONE;
      const data = this.authService.generateJWT(user);

      throw {
        data,
        code
      };
    }
  }

  async isValidVerification(phoneNumber: string, code: string) {
    const { status } = await this.verify(phoneNumber, code);

    return status === APPROVED;
  }

  async confirmDelete(userId: string) {
    const { phone_number } = await this.userService.getSingleUser(userId);

    await this.sendSms(phone_number);
  }

  async sendSms(phoneNumber: string) {
    const { to, status } = await this.send(phoneNumber);

    if (status === PENDING || status === APPROVED) {
      return { to };
    }

    if (status === BAD_REQUEST) {
      throw {
        code: INVALID_NUMBER
      };
    }

    throw {
      code: FAILURE
    };
  }

  async verifyByCode(phoneNumber: string, code: string) {
    const { status } = await this.verify(phoneNumber, code);

    if (status === APPROVED) {
      await this.checkForDuplicate(phoneNumber);

      return this.userService.createUser(phoneNumber);
    }

    if (status === PENDING) {
      throw {
        code: INVALID_CODE
      };
    }

    throw {
      code: FAILURE
    };
  }
}
