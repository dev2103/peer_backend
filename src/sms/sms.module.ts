import { Module } from '@nestjs/common';
import { SmsService } from './sms.service';
import { SmsController } from './sms.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from 'src/user/user.module';
import { AuthModule } from 'src/auth/auth.module';
import { UserService } from 'src/user/user.service';
import { AuthService } from 'src/auth/auth.service';
import { TwilioModule } from '@lkaric/twilio-nestjs';
import { getTwillioOptions } from 'src/utils/env.utils';
import { userModelDefinition } from 'src/user/user.model';

@Module({
  imports: [
    AuthModule,
    UserModule,
    TwilioModule.register(getTwillioOptions()),
    MongooseModule.forFeature([userModelDefinition])
  ],
  controllers: [SmsController],
  providers: [SmsService, UserService, AuthService],
  exports: [SmsService]
})
export class SmsModule {}
