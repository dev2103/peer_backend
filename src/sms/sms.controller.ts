import { Req, Body, Controller, UseGuards, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { responseDescription } from 'src/utils/swagger.utils';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { apiResponse } from 'src/utils/response.utils';
import { smsCheckDto } from './dto/smsCheck.dto';
import { smsSendDto } from './dto/smsSend.dto';
import { SmsService } from './sms.service';

@ApiTags('Sms')
@Controller('sms')
export class SmsController {
  constructor(private readonly smsService: SmsService) {}

  @Post('send')
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'Phone number that recieved the sms code',
      INVALID_NUMBER: 'The provided phone number is not valid'
    })
  )
  SendSms(@Body() { phoneNumber }: smsSendDto) {
    return apiResponse(() => this.smsService.sendSms(phoneNumber));
  }

  @Post('check')
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'Phone number that recieved the sms code',
      INVALID_CODE: 'The provided verification code is not valid',
      DUPLICATE_PHONE: 'Existing user account'
    })
  )
  checkCode(@Body() { phoneNumber, code }: smsCheckDto) {
    return apiResponse(() => this.smsService.verifyByCode(phoneNumber, code));
  }

  @Post('delete')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'Delete confirmation sms sent',
      NOT_FOUND: 'User is not found'
    })
  )
  confirmRemoveUser(@Req() { user }: any) {
    return apiResponse(() => this.smsService.confirmDelete(user.id));
  }
}
