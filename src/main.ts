import { AppModule } from './app.module';
import { setup, create, serve } from './utils/setup.util';

const bootstrap = () =>
  create(AppModule)
    .then(setup)
    .then(serve);

bootstrap();
