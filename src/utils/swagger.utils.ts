import { Statuses } from './response.utils';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const formatStatuses = (obj: Statuses) =>
  Object.entries(obj)
    .map(([name, value]) => `<strong>${name}:</strong> ${value}`)
    .join('<br/>'.repeat(2));

export const responseDescription = (statuses: Statuses) => {
  const description = formatStatuses({
    ...statuses,
    FAILURE: 'Server failure'
  });

  return { description };
};

export const useSwaggerDocs = (app: NestExpressApplication) => {
  const options = new DocumentBuilder()
    .setDescription('The peer API documentation')
    .setTitle('Peer api')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('doc', app, document);
};
