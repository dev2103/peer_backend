import {
  Transport,
  RedisOptions,
  ClientProviderOptions
} from '@nestjs/microservices';
import { DotenvParseOutput, parse } from 'dotenv';
import { resolve } from 'path';
import { readFileSync } from 'fs';
import { NestApplicationOptions } from '@nestjs/common';
import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';
import { TwilioConfig } from '@lkaric/twilio-nestjs/dist/module/twilio.config';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { NestApplicationContextOptions } from '@nestjs/common/interfaces/nest-application-context-options.interface';

const getEnvName = () =>
  !process.env.NODE_ENV ? '.env' : `.env.${process.env.NODE_ENV}`;

const getEnvPath = () => resolve(process.cwd(), getEnvName());

export const getEnv = () => parse(readFileSync(getEnvPath()));

const env: DotenvParseOutput = getEnv();

export const getServerOptions = (): NestApplicationOptions => {
  if (!process.env.NODE_ENV) {
    return {};
  }

  try {
    const key: Buffer = readFileSync('./server.key');
    const cert: Buffer = readFileSync('./server.cert');

    return {
      httpsOptions: {
        key,
        cert
      }
    };
  } catch (err) {
    return {};
  }
};

export const getRedisOptions = (): NestApplicationContextOptions &
  RedisOptions => {
  const { REDIS: transport } = Transport;
  const { REDIS_PORT, REDIS_HOST } = env;
  const url = `redis://${REDIS_HOST}:${REDIS_PORT}`;

  return {
    transport,
    options: {
      url
    }
  };
};

export const getConfigOptions = (): ConfigModuleOptions => ({
  envFilePath: getEnvPath(),
  isGlobal: true
});

export const getMulterOptions = (): MulterOptions => ({
  dest: './uploads'
});

export const getRedisProviderOptions = (): ClientProviderOptions => {
  const { REDIS_PORT, REDIS_HOST, REDIS_NAME } = env;

  const url = `redis://${REDIS_HOST}:${REDIS_PORT}`;

  return {
    name: REDIS_NAME,
    transport: Transport.REDIS,
    options: {
      url
    }
  };
};

export const getMongooseUri = (): string => {
  const { MONGO_NAME, MONGO_PORT, MONGO_HOST } = env;

  return `mongodb://${MONGO_HOST}:${MONGO_PORT}/${MONGO_NAME}`;
};

export const getMongooseOptions = () => ({
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false
});

export const getTwillioOptions = (): TwilioConfig => {
  const { TWILLIO_ID, TWILLIO_TOKEN } = env;

  return {
    accountSid: TWILLIO_ID,
    authToken: TWILLIO_TOKEN
  };
};
