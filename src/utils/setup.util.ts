import { join } from 'path';
import { NestFactory } from '@nestjs/core';
import * as compression from 'compression';
import { useSwaggerDocs } from './swagger.utils';
import { getEnv, getServerOptions } from './env.utils';
import { NestExpressApplication } from '@nestjs/platform-express';

const useStaticFolder = (app: NestExpressApplication) => {
  const staticDir = join(__dirname, '..', '..', 'files');
  const prefix = '/files/';

  app.useStaticAssets(staticDir, {
    prefix
  });
};

const usePrefix = (app: NestExpressApplication) => {
  const { APP_PREFIX = 'api' } = getEnv();
  app.setGlobalPrefix(APP_PREFIX);
};

const useCompression = (app: NestExpressApplication) => app.use(compression());

export const setup = (app: NestExpressApplication) => {
  useCompression(app);
  usePrefix(app);
  useStaticFolder(app);
  useSwaggerDocs(app);
  return app;
};

export const create = (appModule: any) =>
  NestFactory.create<NestExpressApplication>(appModule, getServerOptions());

export const serve = (app: NestExpressApplication) => {
  const { APP_PORT = 3000, APP_HOST = '0.0.0.0' } = getEnv();
  app.listen(APP_PORT, APP_HOST).then(async () => {
    const url = await app.getUrl();
    console.log(`Application is running on: ${url}`);
  });
};
