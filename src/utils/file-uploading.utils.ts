import { extname, join } from 'path';
import { unlinkSync } from 'fs';
import { Request } from 'express';
import { getEnv } from './env.utils';
import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';

const isValidImage = (mimetype: string) =>
  mimetype === 'image/jpg' ||
  mimetype === 'image/jpeg' ||
  mimetype === 'image/png';

export const imageFileFilter = (
  req: Request,
  { mimetype }: Express.Multer.File,
  cb: (err: Error | null, acceptFile: boolean) => void
): void => {
  if (isValidImage(mimetype)) {
    cb(null, true);
  } else {
    const error = new Error('Image uploaded is not of type jpg/jpeg or png');
    cb(error, false);
  }
};

const rand = (max = 1) => ~~(Math.random() * max);

const randomChar = () => rand(16).toString(16);

const generateUid = (length = 4) =>
  Array(length)
    .fill(null)
    .map(randomChar)
    .join('');

export const editFileName = (
  req: Request,
  { originalname }: Express.Multer.File,
  cb: (err: Error | null, file: string) => void
): void => {
  const uid = generateUid();
  const ext = extname(originalname);
  const name = originalname.split('.')[0];
  const fileName = `${name}-${uid}${ext}`;

  cb(null, fileName);
};

export const deleteOldAvatar = (avatar: string): void => {
  if (avatar) {
    const { APP_DOMAIN } = getEnv();
    const path = join(process.cwd(), avatar.replace(`${APP_DOMAIN}/`, ''));

    try {
      unlinkSync(path);
    } catch (error) {
      console.log(`cannot delete old avatar: ${path}`);
    }
  }
};

export const makeInterceptor = (name: string) =>
  FileInterceptor(name, {
    storage: diskStorage({
      destination: './files',
      filename: editFileName
    }),
    fileFilter: imageFileFilter
  });
