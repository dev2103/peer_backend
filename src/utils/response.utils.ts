interface ApiResponse {
  data?: any;
  status: string;
}

export interface Statuses {
  readonly SUCCESS?: string;
  readonly FAILURE?: string;
  readonly NOT_FOUND?: string;
  readonly BAD_REQUEST?: string;
  readonly INVALID_CODE?: string;
  readonly INVALID_NUMBER?: string;
  readonly DUPLICATE_PHONE?: string;
}

// Responses
export const responseStatuses: Statuses = {
  DUPLICATE_PHONE: 'DUPLICATE_PHONE',
  INVALID_NUMBER: 'INVALID_NUMBER',
  INVALID_CODE: 'INVALID_CODE',
  BAD_REQUEST: 'BAD_REQUEST',
  NOT_FOUND: 'NOT_FOUND',
  FAILURE: 'FAILURE'
};

const success = (data?: any): ApiResponse => ({
  ...(data && { data }),
  status: 'SUCCESS'
});

const failure = (err: any) => {
  const { FAILURE } = responseStatuses;
  const { code, message, data } = err;
  const status = responseStatuses[code] || FAILURE;

  console.error(err);

  return {
    status,
    ...(data && { data }),
    ...(message && { message })
  };
};

export const apiResponse = (cb: () => Promise<any>) =>
  cb()
    .then(success)
    .catch(failure);
