export const cleanObject = (obj: any, backup: boolean = false) => {
  const { __v, _id: id, backupKey, ...data } = obj.toObject();

  return {
    id,
    ...data,
    ...(backup && { backupKey })
  };
};

export const Clean = decoratorFactory(cleanObject);

function decoratorFactory(cb: (args: any) => any) {
  return function() {
    return function(target: any, key: string, descriptor: PropertyDescriptor) {
      const originalMethod = descriptor.value;

      descriptor.value = async function(...args: any) {
        return cb(await originalMethod.apply(this, args));
      };

      return descriptor;
    };
  };
}
