import {
  Prop,
  SchemaFactory,
  ModelDefinition,
  Schema as SchemaDeco
} from '@nestjs/mongoose';
import { skills } from './score.interface';
import { Document, Schema } from 'mongoose';

@SchemaDeco({
  timestamps: true
})
export class Score extends Document {
  @Prop({
    ref: 'User',
    required: true,
    type: Schema.Types.ObjectId
  })
  to!: string;

  @Prop({
    ref: 'User',
    required: true,
    type: Schema.Types.ObjectId
  })
  from!: string;

  @Prop({
    type: String,
    enum: skills,
    required: true
  })
  skill!: string;

  @Prop({
    min: 1,
    max: 10,
    type: Number,
    required: true
  })
  value!: number;
}

export const scoreSchema = SchemaFactory.createForClass(Score);

export const scoreModelDefinition: ModelDefinition = {
  name: 'Score',
  schema: scoreSchema
};
