export interface Score {
  readonly UNDERSTANDING?: number;
  readonly RESPECTFUL?: number;
  readonly OPTIMIST?: number;
  readonly LISTENER?: number;
  readonly REACTIVE?: number;
  readonly RELIABLE?: number;
  readonly HUMBLE?: number;
  readonly HONEST?: number;
  readonly LOYAL?: number;
  readonly FAIR?: number;
  readonly KIND?: number;
}

export interface Skill {
  readonly name: string;
  readonly value: number;
}

export interface SkillDescription {
  readonly name: string;
  readonly color: string;
}

export interface Description {
  readonly color: string;
  readonly skills: SkillDescription[];
}

export interface SkillDescriptionMap {
  readonly groupColor: string;
  readonly color: string;
  readonly average?: number;
  readonly skill?: string;
}

export interface DescriptionMap {
  readonly UNDERSTANDING?: SkillDescriptionMap;
  readonly RESPECTFUL?: SkillDescriptionMap;
  readonly OPTIMIST?: SkillDescriptionMap;
  readonly LISTENER?: SkillDescriptionMap;
  readonly REACTIVE?: SkillDescriptionMap;
  readonly RELIABLE?: SkillDescriptionMap;
  readonly HUMBLE?: SkillDescriptionMap;
  readonly HONEST?: SkillDescriptionMap;
  readonly LOYAL?: SkillDescriptionMap;
  readonly FAIR?: SkillDescriptionMap;
  readonly KIND?: SkillDescriptionMap;
}

export const skills = [
  'UNDERSTANDING',
  'RESPECTFUL',
  'OPTIMIST',
  'LISTENER',
  'REACTIVE',
  'RELIABLE',
  'HUMBLE',
  'HONEST',
  'LOYAL',
  'FAIR',
  'KIND'
];
