import { Module } from '@nestjs/common';
import { ScoreService } from './score.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ScoreController } from './score.controller';
import { scoreModelDefinition } from './score.model';

@Module({
  imports: [MongooseModule.forFeature([scoreModelDefinition])],
  controllers: [ScoreController],
  providers: [ScoreService]
})
export class ScoreModule {}
