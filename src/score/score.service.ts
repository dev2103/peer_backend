import {
  Description,
  DescriptionMap,
  SkillDescriptionMap,
  Score as UserScore
} from './score.interface';
import { Score } from './score.model';
import { Model, Types } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as desc from './score.definition.json';
import { ScoreCreateDto } from './dto/scoreCreate.dto';

const expandDesc = (desc: Description[]): DescriptionMap =>
  desc.reduce(
    (acc, { color: groupColor, skills }) => ({
      ...acc,
      ...skills.reduce((acc, { name, color }) => {
        acc[name] = {
          groupColor,
          color
        };

        return acc;
      }, {})
    }),
    {}
  );

@Injectable()
export class ScoreService {
  private expandedDesc = expandDesc(desc);

  constructor(
    @InjectModel('Score')
    private readonly scoreModel: Model<Score>
  ) {}

  private async updateScore(
    from: string,
    { to, value, skill }: ScoreCreateDto
  ) {
    const existingReview = await this.scoreModel.findOne({
      skill,
      from,
      to
    });

    if (!existingReview) {
      return null;
    }

    existingReview.value = value;

    return await existingReview.save();
  }

  private async createScore(from: string, score: ScoreCreateDto) {
    const review = new this.scoreModel({
      from,
      ...score
    });

    return await review.save();
  }

  private formatScores(scores: any[]): UserScore {
    return scores.reduce((acc, { _id, value }) => {
      acc[_id] = +value.toFixed(2);
      return acc;
    }, {});
  }

  private fillMissingFields(score: UserScore): SkillDescriptionMap[] {
    return Object.entries(this.expandedDesc).map(
      ([skill, { groupColor, color }]) => ({
        skill,
        color,
        groupColor,
        average: score[skill] || 5
      })
    );
  }

  private getTime(period: string) {
    const currentTimestamp = new Date().getTime();
    const dayInMs = 60 * 60 * 24 * 1000;
    const periods = {
      WEEK: 7,
      MONTH: 30,
      SIX_MONTHS: 6 * 30
    };

    const numberOfDays = periods[period] || 7;

    return new Date(currentTimestamp - numberOfDays * dayInMs);
  }

  async addScore(from: string, score: ScoreCreateDto) {
    if (from === score.to) {
      throw new Error("You can't review yourself");
    }

    if (!(await this.updateScore(from, score))) {
      await this.createScore(from, score);
    }
  }

  async getScore(to: string, period: string) {
    const $match = {
      to: Types.ObjectId(to),
      updatedAt: {
        $gte: this.getTime(period)
      }
    };

    const $group = {
      _id: '$skill',
      value: {
        $avg: '$value'
      }
    };

    const score = await this.scoreModel.aggregate([{ $match }, { $group }]);

    return this.fillMissingFields(this.formatScores(score));
  }
}
