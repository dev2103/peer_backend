import { IsNumber, IsString, IsNotEmpty } from 'class-validator';
import { skills } from '../score.interface';
import { ApiProperty } from '@nestjs/swagger';

export class ScoreCreateDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: 'The user to recieve the score'
  })
  readonly to!: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    type: String,
    enum: skills,
    description: 'The skill to review'
  })
  readonly skill!: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    type: Number,
    description: 'The score for that given skill'
  })
  readonly value!: number;
}
