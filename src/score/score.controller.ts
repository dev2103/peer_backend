import {
  Get,
  Req,
  Body,
  Post,
  Query,
  Param,
  UseGuards,
  Controller
} from '@nestjs/common';
import {
  ApiTags,
  ApiQuery,
  ApiBearerAuth,
  ApiOkResponse
} from '@nestjs/swagger';
import { ScoreService } from './score.service';
import { apiResponse } from 'src/utils/response.utils';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ScoreCreateDto } from './dto/scoreCreate.dto';
import { responseDescription } from '../utils/swagger.utils';

const periods = ['WEEK', 'MONTH', 'SIX_MONTHS'];

@ApiTags('Score')
@Controller('score')
export class ScoreController {
  constructor(private readonly scoreService: ScoreService) {}

  // ? Get a user scores
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @ApiQuery({
    type: String,
    enum: periods,
    name: 'period',
    required: false
  })
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'List of all reviews'
    })
  )
  getScore(@Param('id') userId: string, @Query('period') period: string) {
    return apiResponse(() => this.scoreService.getScore(userId, period));
  }

  // ? Create a new review
  @Post()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse(
    responseDescription({
      SUCCESS: 'Score added succesfully'
    })
  )
  addScore(@Body() score: ScoreCreateDto, @Req() { user }: any) {
    return apiResponse(() => this.scoreService.addScore(user.id, score));
  }
}
