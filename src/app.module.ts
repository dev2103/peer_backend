import {
  getMongooseUri,
  getConfigOptions,
  getMulterOptions,
  getMongooseOptions
} from './utils/env.utils';
import { Module } from '@nestjs/common';
import { SmsModule } from './sms/sms.module';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ScoreModule } from './score/score.module';
import { EventsModule } from './events/events.module';
import { MulterModule } from '@nestjs/platform-express';
import { RedisCacheModule } from './redis-cache/redis-cache.module';

@Module({
  imports: [
    SmsModule,
    UserModule,
    ScoreModule,
    EventsModule,
    RedisCacheModule,
    ConfigModule.forRoot(getConfigOptions()),
    MulterModule.register(getMulterOptions()),
    MongooseModule.forRoot(getMongooseUri(), getMongooseOptions())
  ]
})
export class AppModule {}
