#!/bin/bash

# any future command that fails will exit the script
set -e

# Lets write the public key of our server
eval $(ssh-agent -s)
ssh-add <(echo "$SSH_PRIVATE_KEY" | base64 -d)

# disable the host key checking.
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

echo "Test ssh connection"
ssh -T "$TARGET_SERVER_USER@$TARGET_SERVER_HOST"

# Delploy
echo "Setup target server directories"
pm2 deploy ecosystem.config.js development setup 2>&1 || true
echo "make deploy"
pm2 deploy ecosystem.config.js development update