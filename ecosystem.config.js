const PEER_APP = {
	name: 'peer-app',
	script: 'npm run start:prod',
	exec_mode: 'fork',
	env_development: {
		NODE_ENV: 'dev'
	},
	env_production: {
		NODE_ENV: 'production'
	}
};

const envConf = type => {
	const ref = 'origin/master';
	const ssh_options = 'StrictHostKeyChecking=no';
	const repo = 'git@gitlab.com:dev2103/peer_backend.git';
	const path = '/var/www/html/hello/backend-server/peer_backend';

	const host = process.env.TARGET_SERVER_HOST
		? process.env.TARGET_SERVER_HOST.trim()
		: "";

	const user = process.env.TARGET_SERVER_USER
		? process.env.TARGET_SERVER_USER.trim()
		: "";

	return {
		ref,
		user,
		host,
		repo,
		path,
		ssh_options,
		"post-deploy":
			`echo "Building for ${ type }"` +
			'npm install && ' +
			'npm run build && ' +
			`pm2 startOrRestart ${ path }/ecosystem.config.js --env ${ type } && ` +
			'pm2 save'
	};
};

module.exports = {
	apps: [ PEER_APP ],
	deploy: {
		development: envConf('development'),
		production: envConf('production')
	}
};